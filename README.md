# CesiumJs TypeScript Starter


## Setup Dev Container with Docker

To start Developing, you can start the provided Dev-Container:

```
docker-compose run --rm -p3000:3000 dev
```
## Install Build-Dependencies

To make sure alle build-dependencies are installed, run:

```
npm install
```

## Run Dev-Server

To start a [DevServer](https://webpack.js.org/configuration/dev-server/), run

```
npm start
```

After startup, you can access it on [http://localhost:3000/](http://localhost:3000/)

## Adding new NPM Modules in Dev Container

When you stop the dev service after running ```npm install <package-name> [--save-dev]```,
the last installed node_modules are not perstisted until the image of the dev-container is rebuild.
Toi persist those changes you may want to rebuild this image after installing new dependencies with

```bash
docker-compose build --no-cache dev # rebuild, while making sure it doesn't use the internal cache 
```