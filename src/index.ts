import { Viewer, WebMercatorProjection, SceneMode, Cartesian3, Ellipsoid, Cartographic, Math as CMath, CesiumWidget, BaseLayerPicker, TileMapServiceImageryProvider, buildModuleUrl, ProviderViewModel, OpenStreetMapImageryProvider, IonImageryProvider, createWorldImagery, IonWorldImageryStyle, ArcGisMapServerImageryProvider } from 'cesium';
import 'cesium/../Widgets/widgets.css'
import './css/main.css';

var imageryViewModels = [];

const offlineProviderViewModel = new ProviderViewModel({
    name: 'Natural Earth\u00a0II',
    iconUrl: buildModuleUrl('Widgets/Images/ImageryProviders/naturalEarthII.png'),
    tooltip: 'Natural Earth II, darkened for contrast.\nhttp://www.naturalearthdata.com/',
    creationFunction: function () {
        return new TileMapServiceImageryProvider({
            url: buildModuleUrl('Assets/Textures/NaturalEarthII')
        });
    }
})

imageryViewModels.push(offlineProviderViewModel);

imageryViewModels.push(new ProviderViewModel({
    name: 'Open\u00adStreet\u00adMap',
    iconUrl: buildModuleUrl('Widgets/Images/ImageryProviders/openStreetMap.png'),
    tooltip: 'OpenStreetMap (OSM) is a collaborative project to create a free editable \
map of the world.\nhttp://www.openstreetmap.org',
    creationFunction: function () {
        return new OpenStreetMapImageryProvider({
            url: 'https://a.tile.openstreetmap.org/'
        });
    }
}));

// from createDefaultImageryProviderViewModels.js in cesium:
imageryViewModels.push(
    new ProviderViewModel({
        name: "Bing Maps Aerial",
        iconUrl: buildModuleUrl("Widgets/Images/ImageryProviders/bingAerial.png"),
        tooltip: "Bing Maps aerial imagery, provided by Cesium ion",
        category: "Cesium ion",
        creationFunction: function () {
            return createWorldImagery({
                style: IonWorldImageryStyle.AERIAL,
            });
        },
    })
);

imageryViewModels.push(
    new ProviderViewModel({
        name: "Bing Maps Aerial with Labels",
        iconUrl: buildModuleUrl(
            "Widgets/Images/ImageryProviders/bingAerialLabels.png"
        ),
        tooltip: "Bing Maps aerial imagery with labels, provided by Cesium ion",
        category: "Cesium ion",
        creationFunction: function () {
            return createWorldImagery({
                style: IonWorldImageryStyle.AERIAL_WITH_LABELS,
            });
        },
    })
);

imageryViewModels.push(
    new ProviderViewModel({
        name: "ESRI World Imagery",
        iconUrl: buildModuleUrl(
            "Widgets/Images/ImageryProviders/esriWorldImagery.png"
        ),
        tooltip:
            "\
World Imagery provides one meter or better satellite and aerial imagery in many parts of the world and lower resolution \
satellite imagery worldwide.  The map includes NASA Blue Marble: Next Generation 500m resolution imagery at small scales \
(above 1:1,000,000), i-cubed 15m eSAT imagery at medium-to-large scales (down to 1:70,000) for the world, and USGS 15m Landsat \
imagery for Antarctica. The map features 0.3m resolution imagery in the continental United States and 0.6m resolution imagery in \
parts of Western Europe from DigitalGlobe. In other parts of the world, 1 meter resolution imagery is available from GeoEye IKONOS, \
i-cubed Nationwide Prime, Getmapping, AeroGRID, IGN Spain, and IGP Portugal.  Additionally, imagery at different resolutions has been \
contributed by the GIS User Community.\nhttp://www.esri.com",
        category: "Other",
        creationFunction: function () {
            return new ArcGisMapServerImageryProvider({
                url:
                    "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
                enablePickFeatures: false,
            });
        },
    })
);

const viewer = new Viewer('cesiumContainer',{
    //Start in Columbus Viewer
    sceneMode : SceneMode.COLUMBUS_VIEW,
    // Show Columbus View map with Web Mercator projection
    mapProjection : new WebMercatorProjection(),
    imageryProviderViewModels: imageryViewModels,
});

viewer.camera.percentageChanged = 0.1;
viewer.camera.changed.addEventListener(function(_percentageChanged) {
    const { latitude: lat, longitude: lon } = viewer.camera.positionCartographic;
    const format = (degree: number, precision: number = 1e4) => 
        Math.round( (CMath.toDegrees(degree) + Number.EPSILON) * precision) / precision;
    // Update window-title with current camera-position
    document.title = `${lat>=0?'N':'S'}${format(lat)}, ${lon>=0?'E':'W'}${format(lon)}`;
})

if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition((position) => {
        const{ coords: {latitude, longitude, altitude, heading} } = position;
        viewer.camera.setView({
            destination : Cartesian3.fromDegrees(
                longitude,
                latitude,
                10_000 //m
                //Ellipsoid.WGS84.cartesianToCartographic(viewer.camera.position).height
            )
        });
    })
}